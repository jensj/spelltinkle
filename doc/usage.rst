Usage
=====

::
    
    $ spelltinkle [-h] [-w] [-S] [-D] [files [files ...]]

Optional arguments:
    
-h, --help        Show this help message and exit
-w, --new-window  Open a new window
-S, --self-test   Run self-test
-D, --debug       Run in debug-mode

