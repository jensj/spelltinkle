Spelltinkle
===========

Simple text editor in a terminal.

.. note::

    **work in progress**

    Don't use it unless you know what you are doing (and if you are not me,
    you don't).


.. toctree::
   :maxdepth: 2

   install
   development
   api


.. image:: screenshot.png


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
