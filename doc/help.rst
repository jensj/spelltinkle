Moving:

=======  =====================
`Ctrl+a`     beginning of line
`Ctrl+a Ctrl+a`  beginning of document
`Ctrl+e`     end of line
`Ctrl+e Ctrl+e`  end of document
`Ctrl+f`  search forward
`Ctrl+x Ctrl+f`  search and replace
`Ctrl+r`  search backwards
`Ctrl+g`  go to next error
=======  =====================

Blocks:

=======  ===============
`Ctrl+b Ctrl+p`  paste
`Ctrl+b Ctrl+b`
`Ctrl+b Ctrl+d`  delete
`Ctrl+b <`   dedent 4 spaces
`Ctrl+b >`   indent 4 spaces
=======  ===============

`Ctrl+<space>`  normalize space
`Ctrl+c`  copy marked region
`Ctrl+p`  paste
`Ctrl+w`  mark word under cursor
`Ctrl+d`  delete charcter under cursor or marked region
`Ctrl+h`  help
`Ctrl+k`  delete to end of line
`Ctrl+o`  open file
`Ctrl+s`  save file
`Ctrl+x Ctrl+s`  save as
`Ctrl+q`  close tab
`Ctrl+x Ctrl+q`  quit!

`Ctrl+t`          open tab list
`Ctrl+t Ctrl+t`       swich to previous tab

`Ctrl+x +`  uppercase
`Ctrl+x -`  lowercase
`Ctrl+x Ctrl+u`  undo
`Ctrl+x Ctrl+r`  redo

`Ctrl+y s`  check spelling
`Ctrl+y y`  format Python code
`Ctrl+y g`  go to definition
`Ctrl+y c`  complete word
`Ctrl+y f`  format text
