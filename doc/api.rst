API
===


.. automodule:: spelltinkle.actions
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: spelltinkle.analysis
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: spelltinkle.choise
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: spelltinkle.color
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: spelltinkle.complete
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: spelltinkle.document
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: spelltinkle.exceptions
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: spelltinkle.fileinput
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: spelltinkle.filelist
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: spelltinkle.fromimp
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: spelltinkle.game
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: spelltinkle.help
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: spelltinkle.history
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: spelltinkle.input
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: spelltinkle.keys
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: spelltinkle.replace
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: spelltinkle.run
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: spelltinkle.screen
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: spelltinkle.search
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: spelltinkle.session
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: spelltinkle.view
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: spelltinkle
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: spelltinkle.test.screen
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: spelltinkle.test.selftest
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: spelltinkle.test.tests
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: spelltinkle.test
    :members:
    :undoc-members:
    :show-inheritance:
