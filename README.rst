Spelltinkle
===========

Simple text editor in a terminal.

::

    ┌────┐
    │SPEL│
    │LTIN│
    │KLE │
    └────


Use
---

To edit a file, use::

    $ spelltinkle file.py

or::

    $ alias e=spelltinkle
    $ e file.py

Type ``spelltinkle -h`` for help.
